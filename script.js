var greet = function () {
var balance_time;
var total_time =  moment.duration("00:00:00");
var url      = window.location.href;     // Returns full URL (https://example.com/path/example.html)
// var pathname = window.location.pathname; // Returns path only (/path/example.html)
// var origin   = window.location.origin;   // Returns base URL (https://example.com)
if(url == "https://mytimesheet.in/dashboard")
{
    let getTime =$(".workingtime_disp_inner div:eq(1) p").html();
    let time1 = moment("01:00", "HH:mm");
    let time2 = moment(getTime, "HH:mm");
    let diff = moment.duration(time1.diff(time2));
    let result = moment.utc(diff.asMilliseconds()).format("HH:mm");
  
    let html=`
                <div class="working_time_sc">
                    <p>${result}</p>
                    <span>&nbsp; remaining Time</span>
                </div>
             `;

    $('#working_time_wrpr .workingtime_disp_inner').append(html);
    // $(".workingtime_disp:after").css("display","none");


    // $(".break_timing_outer tbody tr").length

    if($("tr td:nth-child(3)").length != 0)
    {
        $("tr td:nth-child(3)").each(function() {
            let value = $(this).text();
            value = value.replace(/\n/g, "");

            let durationComponents = value.split(":");
            let duration = moment.duration({
                minutes: parseInt(durationComponents[0]),
                seconds: parseInt(durationComponents[1])
            });
            let formattedDuration = moment.utc(duration.asMilliseconds()).format("HH:mm:ss");

            let sum = moment.duration(total_time).add(formattedDuration);
            let formattedSum = moment.utc(sum.asMilliseconds()).format("HH:mm:ss");
            total_time=formattedSum;
        
        });
        
        let defalt_time=moment.duration("01:00:00");
        let defalt_time1=moment.duration(total_time);

         balance_time =moment.duration(defalt_time).subtract(defalt_time1);
         balance_time = moment.utc(balance_time.asMilliseconds()).format("HH:mm:ss");

         let html1=`<tr class="break_table_bottom" id="current_break_duration_container1">
         <td colspan="2" style="text-align:left;padding-left:5%"><p class="cur_breaktime_heading"> Remaining Break Time </p></td>
         <td>
             <span class="classcur_breaktime_heading" id="">${balance_time}</span>
         </td>
        </tr>`;

     $('.break_timing_inner table:nth-child(2) tbody').append(html1)

    }

    let current_comp_exp = $('.current_comp_exp .add_content:last').html()
    let cur_year = parseFloat(current_comp_exp);
    cur_year = cur_year.toString();
    let pre_year= '1.1';
    let tot_year =parseInt(pre_year) +parseInt(cur_year);
    let pre_year_1 = afterDotDigit(pre_year);
    let cur_year_1 = afterDotDigit(cur_year);
    let tot_month =parseInt(pre_year_1)+parseInt(cur_year_1);
    let t_count=0;let tot_ex=0;
    if(tot_month > 11){
        t_count = tot_month%12;
        if(t_count!=10){
            t_count = '1.'+t_count;
            t_count =parseFloat(t_count);
            tot_ex =parseFloat(tot_year)+t_count;
        }else{
            t_count = '1.'+t_count;
            t_count =parseFloat(t_count).toFixed(2);
            tot_ex = (parseFloat(tot_year) + parseFloat(t_count)).toFixed(2);
        }
    }else{
        t_count = tot_month / 10;
        tot_ex =parseFloat(tot_year)+t_count;
    }
       
    let TX_html=`<div class="current_comp_exp">
        <p class="add_header">My Total Experience</p>
        <p class="add_content">${tot_ex} Years</p>
    </div>`;

    $('.additional_1').append(TX_html)
    $('.additional_1').css('grid-template-columns','repeat(5, 1fr)');

}
else if(url == "https://mytimesheet.in/leave/index"){
    $(document).ready(function(){

       
        // var currentDate = moment().format('DD MMM YYYY');
        var currentDay = moment().format('DD');
        var currentMonth = moment().format('MMM');
        currentMonth = moment().month(currentMonth).format('M');
        $(".text-center tr td:nth-child(2)").each(function() {
        let dateString =  $(this).text()
        let dateArray = dateString.split('/');
        let targetDay = dateArray[0];
        let targetMonth = dateArray[1];
        let year = dateArray[2];
        if ( parseInt(currentMonth) < parseInt(targetMonth)) {
               $(this).parent().css('color',"green");
        } 
        else if(parseInt(currentMonth)==parseInt(targetMonth) && parseInt(currentDay) < parseInt(targetDay))
        {
               $(this).parent().css('color',"green");
        }
        else{
               $(this).parent().css('color',"red");
        }
    
        });
    
    })
}
 
};
    greet();

function afterDotDigit(number){
    let parts = number.split('.');
    return decimalPart = parts.length > 1 ? parts[1] : '';
}

